@extends('layouts.app')

@section('content')

	<form action="/posts">
		
		<div class="form-group">
			<label for="title">Title:</label>
			<input type="text" name="title" id="title" class="form-control" value="{{$post->title}}">
		</div><br>

		<div class="form-group">
			<label for="content">Content:</label>
			<textarea name="content" id="content" class="form-control">{{$post->content}}</textarea>
		</div>

		<div class="mt-2">
			@method('PUT')
			@csrf
			<button type="submit" class="btn btn-primary">Update Post</button>
		</div>

	</form>


@endsection